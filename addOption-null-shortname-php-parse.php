<?php

/*
 * Change the second argument of "addOption" to null.
 *
 * Copyright (C) 2017  Antonio Ospite <ao2@ao2.it>
 */

/*
 * php-parse is installed with:
 *  composer global require nikic/php-parser
 */
include getenv('HOME') . '/.config/composer/vendor/autoload.php';

use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter;

class AddOptionSecondArgumentVisitor extends NodeVisitorAbstract {

  public function leaveNode(Node $node) {
    if ($node instanceof Node\Expr\MethodCall && $node->name === "addOption") {

      $shortname_arg = $node->args[1];

      // If it's a constant expression (e.g. false or null) make it null.
      if ($shortname_arg->value instanceof Node\Expr\ConstFetch) {
        $shortname_arg->value->name = new Node\Name("null");
      }
      elseif ($shortname_arg->value instanceof Node\Scalar\String_) {
        // If it's an empty string, make it null keeping the attributes.
        if (empty($shortname_arg->value->value)) {
          $old_attributes = $shortname_arg->value->getAttributes();
          $shortname_arg->value = new Node\Expr\ConstFetch(new Node\Name("null"), $old_attributes);
        }
      }
      else {
        echo "UNEXPECTED argument type\n";
      }
    }
  }

}

$parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
$traverser = new NodeTraverser();
$prettyPrinter = new PrettyPrinter\Standard();

$traverser->addVisitor(new AddOptionSecondArgumentVisitor());

try {
  $code = file_get_contents($argv[1]);
  $stmts = $parser->parse($code);
  $stmts = $traverser->traverse($stmts);

  // $stmts is an array of statement nodes.
  $code = $prettyPrinter->prettyPrintFile($stmts);
  echo $code;

}
catch (Error $e) {
  echo 'Parse Error: ', $e->getMessage();
}
